# -*- coding: utf-8 -*-

module VideoStorage
	require "fileutils"
	require "inifile"

	Config = IniFile.load "/etc/opt/tsweb/video_storage/video_storage.conf"

	def self.silence
		::FFMPEG.logger.level = ::Logger::FATAL
	end

	class Server
		def allocate type
			if type.nil?
				raise "No storage type."
			elsif Config.nil? || Config[type].nil? || Config[type].length == 0
				raise "Configuration value does not exist."
			end
			@storage = Storage::Factory.new(type).create(Config[type])
		end
	end

	module Storage
		class Factory < ::Yotta::Factory
			def create opts=nil
				repository = Resource::Repository::Factory.new(@type).create
				config = Config[@type]
				raise "no config[@type]" if config.nil? || config.length == 0
				deployer = ::Yotta::Protocol::Factory.new(config["remote_protocol"]).create({
					:host => config["remote_host"],
					:port => config["remote_port"],
					:username => config["remote_username"],
					:keys => config["ssh_keys"],
					:passphrase => config["ssh_keys_passphrase"]
				})
				Base.new(opts, repository, deployer)
			end
		end

		class Base
			TempDir = "/tmp/__VideoStorage__"

			def initialize opts, repository, deployer
				@opts = opts
				@repository = repository
				@deployer = deployer
			end

			def update
				raise "Storage object is not ready." unless ready?
				updated = @repository.map do |video|
					tmpdir = "#{TempDir}/#{video.id}/rend-#{video.rendition[:id]}"
					dstdir = "#{@opts["datadir"]}/#{video.id}/rend-#{video.rendition[:id]}"
					transaction = Entry::Transaction.new tmpdir, dstdir
					if !transaction.committed?
						tmpfiles = transaction.entries do |tmpdir|
							Entry.mkdir(tmpdir)
							video.store(tmpdir)
						end
						if tmpfiles.length > 0
							yield(tmpfiles, video) if block_given?
							Entry.mkdir(dstdir)
							transaction.commit
							transaction.results
						else
							nil
						end
					else
						nil
					end
				end
				return updated.select{|renditions|!renditions.nil?}
			end

			def deploy renditions
				raise "Storage#deploy: renditions isnt array." unless renditions.instance_of?(Array)
				raise "Storage#deploy: @deployer is nil." if @deployer.nil?
				raise "Storage#deploy: @opts[datadir] is nil." if @opts["datadir"].nil?
				raise "Storage#deploy: @opts[remote_datadir] is nil." if @opts["remote_datadir"].nil?
				dests = renditions.map do |path|
					path.gsub(Regexp.new("^"+Regexp.escape(TempDir)), @opts["remote_datadir"])
				end
				@deployer.deploy renditions, dests
			end

			private

			def ready?
				!@repository.nil? && !@opts["datadir"].nil?
			end
		end
	end

	module Resource
		module Video
			class Factory < ::Yotta::Factory
				def create opts=nil
					case @type
					when :featurephone
						Featurephone.new opts
					end
				end
			end

			class Featurephone < ::Yotta::FFMPEG::FeaturephoneMovie
				attr_reader :id, :rendition

				def initialize opts
					unless opts.nil?
						super opts[:path]
						@id = opts[:id]
						@rendition = opts[:rendition]
					end
				end

				def store dir
					res = []
					res.concat(store_docomo_low(dir))
					res.concat(store_docomo_high(dir))
					res.concat(store_au(dir))
					res.concat(store_softbank(dir))
				end

				private

				def store_docomo_low dir
					process "#{dir}/docomo_low.3gp", self.class.superclass::PROFILE_DOCOMO_LOW
				end
				def store_docomo_high dir
					process "#{dir}/docomo_high.3gp", self.class.superclass::PROFILE_DOCOMO_HIGH
				end
				def store_au dir
					process "#{dir}/au.3g2", self.class.superclass::PROFILE_AU_DEFAULT
				end
				def store_softbank dir
					process "#{dir}/softbank.3gp", self.class.superclass::PROFILE_SOFTBANK_DEFAULT
				end
			end
		end

		module Repository
			class Factory < ::Yotta::Factory
				def create opts=nil
					case @type
					when :featurephone
						Featurephone.new @type
					end
				end
			end

			class Base
				include Enumerable

				def initialize type
					@provider = []
					@video_factory = Video::Factory.new type
				end

				def each
					@provider.each do |provider|
						provider.provide do |opts|
							yield(@video_factory.create opts)
						end
					end
				end
			end

			module HLSProvider
				def download url, uid
					unless uid.nil?
						dst = "/var/cache/__VideoStorage_#{uid}.mp4"
						tmp = "/tmp/#{::File.basename(dst)}"
						transaction = Entry::Transaction.new(tmp, dst)
						unless transaction.committed?
							transaction.commit if ::Yotta::FFMPEG::HLSMovie.fetch(url, tmp)
						end
						return dst
					end
				end
			end

			class Featurephone < Base
				def initialize type
					super
					if Config.nil? || Config[type].nil? || Config[type].length == 0
						raise "Configuration value does not exists."
					end
					config = Config[type]
					provider = !config["provider"].nil? ? config["provider"] : nil
					raise "Configuration value 'provider' does not exists." if provider.nil?
					providers = provider.split(",")

					if providers.include? "BrightcoveHLS"
						raise "You need Brightcove API-Read-Token." if config["brc_read_api_token"].nil?
						raise "You need Brightcove PublisherID." if config["brc_publisher_id"].nil?
						brc = BrightcoveHLS.new({
							:read_api_token => config["brc_read_api_token"],
							:publisher_id => config["brc_publisher_id"]
						})
						@provider.push brc
					end
					if providers.include? "EquipmediaHLS"
						raise "You need Equipmedia API-Read-Token." if config["eq_read_api_token"].nil?
						raise "You need Equipmedia ContractID." if config["eq_contract_id"].nil?
						eq = EquipmediaHLS.new({
							:read_api_token => config["eq_read_api_token"],
							:contract_id => config["eq_contract_id"]
						})
						@provider.push eq
					end
				end

				class BrightcoveHLS < ::Yotta::API::BrightcoveMedia
					include HLSProvider

					def provide
						items = search({
							:any => ["deliver_for_featurephone:yes"],
							:sort_by => "CREATION_DATE:DESC",
							:video_fields => "id,name,version,lastModifiedDate,iOSRenditions"
						})
						items.map do |item|
							vid = "Brightcove-#{@publisher_id}-#{item["id"]}"
							rend = best_rendition(item)
							path = download(rend["url"], "#{vid}_#{rend["id"]}")
							yield({
								:path => path,
								:id => vid,
								:rendition => {:id => rend["id"]}
							})
						end
					end

					private

					def best_rendition item
						item["IOSRenditions"].max_by {|r| r["encodingRate"].to_i}
					end
				end

				class EquipmediaHLS < ::Yotta::API::EquipmediaMedia
					include HLSProvider

					def provide
						require 'digest/sha1'

						items = search({
							:api_keywords => "deliver_for_featurephone",
							:pageSize => 100,
							:pageNumber => 1
						})
						items.map do |item|
							vid = "Equipmedia-#{@contract_id}-#{item["mid"]}"
							hls_uri = best_rendition(item)
							unless hls_uri.nil?
								rend_id = Digest::SHA1.hexdigest(hls_uri)
								path = download(hls_uri, "#{vid}_#{rend_id}")
								yield({
									:path => path,
									:id => vid,
									:rendition => {:id => rend_id}
								})
							end
						end
					end

					private

					def best_rendition item
						item["movie_url"]["mb_sq"] || item["movie_url"]["mb_lq"]
					end
				end
			end
		end
	end

	module Entry
		def self.mkdir dir
			::FileUtils.mkdir_p dir unless ::Dir.exists? dir
		end

		class Transaction
			def initialize tmp, dst
				@tmp = tmp
				@dst = dst
				@tmp_results = []
			end
			def commit
				::FileUtils.copy_entry @tmp, @dst, true
				::FileUtils.remove_entry_secure @tmp
			end
			def entries
				@tmp_results.concat(yield @tmp)
			end
			def results
				@tmp_results.map do |tmppath|
					tmppath.gsub(Regexp.new("^"+Regexp.escape(@tmp)),@dst)
				end
			end
			def committed?
				::File.exists? @dst
			end
		end
	end
end
