#!/usr/local/bin/ruby
# -*- coding: utf-8 -*-

begin
	require "../../lib/yotta/yotta.rb"
	require "../../lib/video_storage/video_storage.rb"

	::VideoStorage.silence
	server = ::VideoStorage::Server.new
	storage = server.allocate :featurephone
	updates = storage.update do |videos, opts|
		storage.deploy videos
	end
rescue => exc
	e = exc
end

require "cgi"
require "json"

cgi = CGI.new
cgi.out({"type" => "application/json", "charset" => "UTF-8"}){
	if e.nil?
		JSON.generate({:success => true, :updated => !updates.empty?, :updates => updates})
	else
		JSON.generate({:success => false, :err_mss => e})
	end
}
